import { Component, ReactNode } from "react";
import axios from "axios";
import { useState } from "react";

interface Props{
    url:string;
}

export class ComponentClass extends Component{

    state={
        url : '',
        post: ''
     }
    
    
    updateInput(url: string){
    this.setState({url : url})
    }

    updatePost(post: string){
        this.setState({post : post})
    }


   
    getQuote = () => {
        axios({
            method: 'get',
            url: this.state.url
          })
             .then((res)=> {
                this.updatePost(JSON.stringify(res.data, null, 2));
             })
             .catch(error=>{
                this.updatePost('Упс.. что то пошло не так');
                console.log(error.cause);
             })
             .then(()=>{
                console.log("все готово");
             })
      }
      
    render() {
        
    return <div className="componentFunctionStyle">
       <div>
            <input className='url-input' value={this.state.url} placeholder='введите url' id='title' onChange={e => this.updateInput(e.target.value)}/>
            <button onClick={this.getQuote}>Отправить</button>    
            <p>{this.state.post}</p>
        </div>
    </div>;
    }
}