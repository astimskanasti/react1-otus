import './componentstyle.css'
import axios from "axios";
import { useState } from "react";

interface Props{
    url:string;
}

export function ComponentFunction(Props : Props)
{
    const componentFunctionStyle = {
        border:"2px solid red",
        fontFamily: "Arial",
        padding: "10px"
    }
    
    const [input, setInput] = useState(Props?.url ?? '');
    const [post, setPost] = useState('');

    
    const getQuote = () => {
        axios({
            method: 'get',
            url: input
          })
             .then((res)=> {
                setPost(JSON.stringify(res.data, null, 2));
             })
             .catch(error=>{
                setPost('Упс.. что то пошло не так');
                console.log(error.cause);
             })
             .then(()=>{
                
                console.log(Props.url);
                console.log("все готово");
             })
      }

      
    return <div>
            <input className='url-input' value={input} placeholder='введите url' id='title' onChange={e => setInput(e.target.value)}/>
            <button onClick={getQuote}>Отправить</button>    
            <p>{post}</p>
        </div>;
}
