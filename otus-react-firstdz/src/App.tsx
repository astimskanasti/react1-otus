import React from 'react';
import logo from './logo.svg';
import './App.css';
import {ComponentFunction} from './Components/ComponentFunction';
import { ComponentClass } from './Components/ComonentClass';

function App() {
  return <>
    <ComponentFunction url={'https://catfact.ninja/fact'}/>
    <ComponentClass/>
  </>;
}

export default App;
